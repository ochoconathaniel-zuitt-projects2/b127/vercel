import {Fragment, useState, useEffect, useContext} from 'react';
import '../components/Login.css'
import { Button } from 'react-bootstrap'
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { Redirect, useHistory } from 'react-router-dom';



export default function Login(){

 		const history = useHistory();

		const {user,setUser} = useContext(UserContext)

		const [email,checkEmail] = useState('');
		const [password,checkPassword] = useState('');


		const [isActive, setIsActive] = useState(false);


		

		useEffect(() => {

			if(email !== '' && password !== ''){

				setIsActive(true);
			}

			else {

				setIsActive(false)
			}


				},[email,password])
	
		

		function loginUser(e){

			e.preventDefault();

		fetch('https://secret-coast-94874.herokuapp.com/user/check', {

			method: 'POST',
			headers: {

				'Content-Type': 'application/json'
			},

			body:JSON.stringify({
				email: email,
				password: password
			})

		})
		.then(res => res.json())
		.then(data => {
				
				if(data.accessToken !== undefined){
						localStorage.setItem('accessToken', data.accessToken);
						setUser({accessToken: data.accessToken});
					





						Swal.fire({

						title: 'Welcome!',
						confirmButtonColor:'black'



					})


					fetch('https://secret-coast-94874.herokuapp.com/user/details', {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				    })
				    .then(res => res.json())
				    .then(data => {
					console.log(data)
					if(data.isAdmin === true){
						localStorage.setItem('email', data.email)
						localStorage.setItem('isAdmin', data.isAdmin)
						setUser({
							email: data.email,
							isAdmin: data.isAdmin,
							_id: data._id
						})
					console.log(user.isAdmin)
						history.push('/products')
					}
					else{
						
						history.push('/')
					}
				})

			}

					



				else {

					Swal.fire({

						title: 'oops!',
						icon: 'error',
						text: 'something went wrong'

  


					})


				}

					checkEmail('')
					checkPassword('')


		})

	}


			






	return (

		<Fragment>
		<body className="login">
		
		<form className="box"  onSubmit ={(e) => loginUser(e)}>
		  <h1>Log in</h1>
		  <input type="text" placeholder="Email" 
		   onChange={e => checkEmail(e.target.value)}></input>
		  <input type="password" placeholder="Password"
		  onChange={e => checkPassword(e.target.value)}></input>
		 
		  {isActive ?
		  <Button type="submit">Submit</Button>	
		  :	
		  <Button type="submit" disabled>Submit</Button>	
		}
		
		</form>	
		

		</body>
		</Fragment>

		)
}		