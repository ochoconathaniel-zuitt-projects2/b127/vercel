import { useState, useEffect, useContext } from 'react';
import { Container } from 'react-bootstrap';
import PantsView from '../components/pants/PantsView';
import UserContext from '../UserContext';

export default function Pants() {
	
	const { user } = useContext(UserContext);

	const [allPants, setAllPants] = useState([])

	const fetchPants = () => {
		fetch('https://secret-coast-94874.herokuapp.com/product/product/pants')
		.then(res => res.json())
		.then(data =>{
			
			setAllPants(data)
		
		})
	}

	useEffect(()=>{
		fetchPants()
	}, [])

	return(
		
		<Container>
			{
			
			 
			 	<PantsView pantData={allPants}/>
			}
		</Container>
		
		)
}