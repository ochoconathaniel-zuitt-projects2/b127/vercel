import {React, Fragment} from 'react';
import CarouselContainer from '../components/CarouselContainer'
import Category from '../components/Category'
import {Link} from 'react-router-dom';


const Home = () => {

	return(

		<Fragment>

			

			<CarouselContainer />
			<Category />

		</Fragment>

		)
}

export default Home;