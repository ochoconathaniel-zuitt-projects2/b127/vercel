import { useState, useEffect, useContext } from 'react';
import AdminView from '../components/AdminView';
import { Container } from 'react-bootstrap';
import UserView from '../components/UserView';
import UserContext from '../UserContext';

export default function Products() {
	
	const { user } = useContext(UserContext);

	const [allProducts, setAllProducts] = useState([])

	const fetchData = () => {
		fetch('https://secret-coast-94874.herokuapp.com/product/get/products')
		.then(res => res.json())
		.then(data =>{
			
			setAllProducts(data)
		
		})
	}

	useEffect(()=>{
		fetchData()
	}, [])

	return(
		
		<Container>
			{
			
			 	(user.isAdmin === true) ?
			 	<AdminView productData={allProducts} fetchData={ fetchData }/>
		     	:
			 	<UserView productData={allProducts}/>
			}
		</Container>
		
		)
}


