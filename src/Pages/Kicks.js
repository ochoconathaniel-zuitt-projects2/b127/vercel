import { useState, useEffect, useContext } from 'react';
import { Container } from 'react-bootstrap';
import ShoesView from '../components/shoes/ShoesView';
import UserContext from '../UserContext';

export default function Kicks() {
	
	const { user } = useContext(UserContext);

	const [allShoes, setAllShoes] = useState([])

	const fetchShoes = () => {
		fetch('https://secret-coast-94874.herokuapp.com/product/product/shoes')
		.then(res => res.json())
		.then(data =>{
			
			setAllShoes(data)
		
		})
	}

	useEffect(()=>{
		fetchShoes()
	}, [])

	return(
		
		<Container>
			{
			
			 
			 	<ShoesView shoeData={allShoes}/>
			}
		</Container>
		
		)
}