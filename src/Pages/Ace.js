import { useState, useEffect, useContext } from 'react';
import { Container } from 'react-bootstrap';
import AceView from '../components/accesories/AceView';
import UserContext from '../UserContext';

export default function Accesories() {
	
	const { user } = useContext(UserContext);

	const [allAccesories, setAllAccesories] = useState([])

	const fetchItems = () => {
		fetch('https://secret-coast-94874.herokuapp.com/product/product/accesories')
		.then(res => res.json())
		.then(data =>{
			
			setAllAccesories(data)
		
		})
	}

	useEffect(()=>{
		fetchItems()
	}, [])

	return(
		
		<Container>
			{
			
			 	<AceView AccesoryData={allAccesories}/>
			
			}
		</Container>
		
		)
}