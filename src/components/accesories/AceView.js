import { useState, useEffect, Fragment} from 'react';
import AceCard from './AceCard';
import {Col, Row} from 'reactstrap'

export default function AceView ({ AccesoryData }){

	const [accesory,setAccesory] = useState([])


	useEffect(()=> {

			const AceArr = AccesoryData.map(item => {

					if(item.isActive === true){

						return(
							< AceCard itemProp={item} key={item._id}/>

							)

					} else {

						return null;
					}




			})


				setAccesory(AceArr)


	},[AccesoryData])

  	return(
  		<Fragment>


  		<Row xs={12} md={3} lg={3} >
  			
  			{accesory}
  		
  		</Row>

  		</Fragment>	


  		)


}