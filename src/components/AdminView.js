import { useState, useEffect, Fragment } from 'react';
import { Table, Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';
import {AiFillFolderAdd} from 'react-icons/ai';
import {AiOutlineEdit} from 'react-icons/ai';
import {GoTrashcan} from 'react-icons/go';
import {Link} from 'react-router-dom';
import './Category.css'


export default function AdminView(props){

	const { productData , fetchData } = props
		
	const [products, setProducts] = useState([])

	const [productName, setName] = useState('');
	const [productDescription, setDescription] = useState('');
	const [productPrice, setPrice] = useState(0);

	const [productId, setProductId] = useState('')

	const [showAdd, setShowAdd] = useState(false);

	const [showEdit, setShowEdit] = useState(false);

	const openAdd = () => setShowAdd(true);
	const closeAdd = () => setShowAdd(false);


	const closeEdit = () => {
		setShowEdit(false)
		setName('')
		setDescription('')
		setPrice(0)
	}




	const openEdit = (productId) => {
		fetch(`https://secret-coast-94874.herokuapp.com/product/${ productId }/`)
		.then(res => res.json())
		.then(data => {
			
			setProductId(data._id)
			setName(data.productName)
			setPrice(data.productPrice)
			setDescription(data.productDescription)
		})

		setShowEdit(true)
	}







useEffect(()=>{
		const productArr = productData.map(product => {
			return(
				<tr key={product._id}>
					<td className="mt-5 " >{product._id}</td>
					<td>{product.productName}</td>
					<td>{product.productDescription}</td>
					<td>{product.productPrice}</td>
					<td className={product.isActive ? "text-success" : "text-danger"}>
						{product.isActive ? "In stock" : "Out of stock" }
					</td>

					<td className="mid">
						
						
						{product.isActive ?
							<Button className="ayo" variant="danger" size="sm" onClick={() => archiveToggle(product._id, product.isActive)}>Disable</Button>
							:
							<Button className="ayo" variant="success" size="sm" onClick={() => activateToggle(product._id, product.isActive)}>Enable</Button>
						}

						<Button className="ayo" variant="dark" size="sm"  onClick={() => openEdit(product._id)}><AiOutlineEdit /></Button>

						<Button className="ayo" variant="dark" onClick={e => deleteProduct(product._id)} size="sm"><GoTrashcan /></Button>

					</td>

					
				</tr>

				)
		})
		setProducts(productArr)
	}, [productData])




// ADD A PRODUCT

const addProduct = (e) => {
		e.preventDefault();
		fetch('https://secret-coast-94874.herokuapp.com/product/createItem', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('accessToken') }`
			},
			body: JSON.stringify({
				productName: productName,
				productPrice: productPrice,
				productDescription: productDescription
				

			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === true){
				
				fetchData()

				Swal.fire({
				
					
					icon: 'success',
					text: 'Product successfully added.',
					confirmButtonColor: 'black'
				})

				setName('')
				setDescription('')
				setPrice(0)
				

				closeAdd()
			}else{
				Swal.fire({
					title: 'Something Went Wrong!',
					icon: 'error',
					text: 'Please Try Again.'
				})
			}
		})
	}





// EDIT PRODUCT




	const editProduct = (e, productId) => {
		e.preventDefault();

		fetch(`https://secret-coast-94874.herokuapp.com/product/${ productId }/`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('accessToken') }`
			},
			body: JSON.stringify({
				productName: productName,
				productPrice: productPrice,
				productDescription: productDescription
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				fetchData()
				Swal.fire({

					icon: 'success',
					text: 'Product has been updated',
					confirmButtonColor: 'black'
				})
				closeEdit()
			}else{
				fetchData()
				Swal.fire({
					title: 'Something Went Wrong!',
					icon: 'error',
					text: 'Please try again'
				})
			}
		})
	





	}



// ARCHIVE PRODUCT


const archiveToggle = (productId, isActive) => {
		fetch(`https://secret-coast-94874.herokuapp.com/product/${ productId }/archive`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('accessToken') }`
			},
			body: JSON.stringify({
				isActive: isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				fetchData()
				Swal.fire({
					
					icon: 'success',
					text: 'Product has been disabled',
					confirmButtonColor: 'black'
				})
			}else{
				fetchData()
				Swal.fire({
					title: 'Something Went Wrong',
					icon: 'error',
					text: 'Please Try again'
				})
			}
		})
	}



// ACTIVATE A PRODUCT


const activateToggle = (productId, isActive) => {
		fetch(`https://secret-coast-94874.herokuapp.com/product/${ productId }/activate`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('accessToken') }`
			},
			body: JSON.stringify({
				isActive: isActive
			})
		})
			.then(res => res.json())
			.then(data => {
			if(data === true){
				fetchData()
				Swal.fire({
					
					icon: 'success',
					text: 'Product is back on stock',
					confirmButtonColor: 'black'
				})
			}else{
				fetchData()
				Swal.fire({
					title: 'Something Went Wrong',
					icon: 'error',
					text: 'Please Try again'
				})
			}
		})
	}


// DELETE A PRODUCT


	const deleteProduct = (productId) => {

		Swal.fire({
 		 title: 'Are you sure you want to delete this Item?',
 		 icon: 'warning',
 		 showCancelButton: true,
  		 confirmButtonColor: 'black',
 		 cancelButtonColor: 'black',
 		 confirmButtonText: 'Yes, Delete'
		 }).then((result) => {
  		 if (result.isConfirmed) {
		


		fetch(`https://secret-coast-94874.herokuapp.com/product/${ productId }/removeItem`, {
			method: 'DELETE',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('accessToken') }`
			}
		})
			.then(res => res.json())
			.then(data => {
			if(data === true){
				fetchData()
				Swal.fire({
					
					icon: 'success',
					text: 'Item has been deleted!',
					confirmButtonColor: 'black'
				})
			}else{
				fetchData()
				Swal.fire({
					title: 'Something Went Wrong',
					icon: 'error',
					text: 'Please Try again'
				})
			}
		})
	

		} else {

			<Link to="/products"></Link>
		}
		
	})


	}



  return(
		<Fragment>
			<div className="text-center mt-4 mb-3">
				<h2 className="mt-4">Admin Dashboard</h2>
				<Button className="mt-3 mb-3" variant="dark" onClick={openAdd}><AiFillFolderAdd /></Button>
				<div className="d-flex justify-content-center">
				</div>
			</div>
			<Table striped bordered hover responsive>
				<thead className="bg-dark text-white">
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th className="text-center">Actions</th>
					</tr>
				</thead>
				<tbody>
					{products}
				</tbody>
			</Table>
	

		
		



				<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={e => addProduct(e)}>
					<Modal.Header>
						<Modal.Title>Create a Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						
						<Form.Group>
							<Form.Label>Name:</Form.Label>
							<Form.Control type="text" value={productName} onChange={e => setName(e.target.value)} required />
						</Form.Group>

						
						<Form.Group>
							<Form.Label>Price:</Form.Label>
							<Form.Control type="text" value={productPrice} onChange={e => setPrice(e.target.value)} required />
						</Form.Group>
						
						
						<Form.Group>
							<Form.Label>Description:</Form.Label>
							<Form.Control type="text" value={productDescription} onChange={e => setDescription(e.target.value)} required />
						</Form.Group>

					</Modal.Body>	

					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>Close</Button>
						<Button variant="dark" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>


					




			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={e => editProduct(e, productId)}>
					<Modal.Header>
						<Modal.Title>Update Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
					
						<Form.Group>
							<Form.Label>Name:</Form.Label>
							<Form.Control type="text" value={productName} onChange={e => setName(e.target.value)} required />
						</Form.Group>

						
						<Form.Group>
							<Form.Label>Price:</Form.Label>
							<Form.Control type="text" value={productPrice} onChange={e => setPrice(e.target.value)} required />
						</Form.Group>
						
						
						<Form.Group>
							<Form.Label>Description:</Form.Label>
							<Form.Control type="text" value={productDescription} onChange={e => setDescription(e.target.value)} required />
						</Form.Group>

					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button variant="dark" type="submit">Submit</Button>
				
					</Modal.Footer>
				
				</Form>
			
			</Modal>






			</Fragment>


		)


}