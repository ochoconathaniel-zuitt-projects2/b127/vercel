import { React, Fragment } from 'react'
import { Link } from 'react-router-dom'
import { Card , Button , Row , Col } from 'react-bootstrap'
import image2 from '../images/sw5.jpg'
import './Category.css'

export default function Category(){
return(
    <Fragment>
    <h4 className="text-center mt-5 ">Check out our new arrivals</h4>
   
    <div className="xop-section">
        <ul className ="xop-grid">
            <li>
            <div className="xop-box xop-img-1">
            <Link to= "/Tops">
                <div className="xop-info">
                <h3>Tops</h3>
                <p>Tees,Hoodies,Crewneck</p>
                </div></Link>
               </div>
              </li>

               <li>
            <div className="xop-box xop-img-2">
            <Link to="/Pants">
                <div className="xop-info">
                <h3>Pants</h3>
                <p>Denim,Track,Cargo</p>
                </div></Link>
               </div>
              </li>
                   

                <li>
            <div className="xop-box xop-img-3">
            <Link to="/Shoes">
               <div className="xop-info">
                <h3>Kicks</h3>
                <p>J's,skating,Limited</p>
                </div></Link>
               </div>
              </li>
                   
                       
                <li>
                <div className="xop-box xop-img-4">
                <Link to="/accesories">
                <div className="xop-info">
                <h3>Accesories</h3>
                <p>Bags,Beanie,utensils</p>
                </div></Link>
               </div>
              </li>
             </ul>
            </div>
                   

    </Fragment>


)


}