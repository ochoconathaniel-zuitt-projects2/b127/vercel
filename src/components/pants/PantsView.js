import { useState, useEffect, Fragment} from 'react';
import PantsCard from './PantsCard';
import {Col, Row} from 'reactstrap'

export default function PantsView ({ pantData }){

	const [pants,setPants] = useState([])


	useEffect(()=> {

			const PantsArr = pantData.map(bottom => {

					if(bottom.isActive === true){

						return(
							< PantsCard pantProp={bottom} key={bottom._id}/>

							)

					} else {

						return null;
					}




			})


				setPants(PantsArr)


	},[pantData])

  	return(
  		<Fragment>


  		<Row xs={12} md={3} lg={3} >
  			
  			{pants}
  		
  		</Row>

  		</Fragment>	


  		)


}