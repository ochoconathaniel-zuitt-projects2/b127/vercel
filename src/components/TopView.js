import { useState, useEffect, Fragment} from 'react';
import TopCard from './TopCard';
import {Col, Row} from 'reactstrap'

export default function TopView ({ shirtData }){

	const [tops,setTops] = useState([])


	useEffect(()=> {

			const TopsArr = shirtData.map(shirt => {

					if(shirt.isActive === true){

						return(
							< TopCard topProp={shirt} key={shirt._id}/>

							)

					} else {

						return null;
					}




			})


				setTops(TopsArr)


	},[shirtData])

  	return(
  		<Fragment>


  		<Row xs={12} md={3} lg={3} >
  			
  			{tops}
  		
  		</Row>

  		</Fragment>	


  		)


}