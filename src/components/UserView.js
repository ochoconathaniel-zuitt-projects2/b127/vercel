import { useState, useEffect, Fragment} from 'react';
import ProductCard from './ProductCard';
import {Col, Row} from 'reactstrap'

export default function UserView ({ productData }){

	const [products,setProducts] = useState([])


	useEffect(()=> {

			const productArr = productData.map(product => {

					if(product.isActive === true){

						return(
							< ProductCard productProp={product} key={product._id}/>

							)

					} else {

						return null;
					}




			})


				setProducts(productArr)


	},[productData])

  	return(
  		<Fragment>


  		<Row xs={12} md={3} lg={3} >
  			
  			{products}
  		</Row>

  		</Fragment>	


  		)


}