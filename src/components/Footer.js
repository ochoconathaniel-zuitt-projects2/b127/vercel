import React from 'react';
import './footer.css'


const Footer = () => {

return(

<div className = "main-footer mt-5">
<div className = "container">
<div className ="row">



<div className = "col">

<ul className = 'list-unstyled'>
<h6>About Us</h6>
<li>Why fall in line to cop the latest apparel releases out there,why settle for overprized when you can order from kyoShiro and co.
From vintage to latest drops we got everything you need right here</li>
</ul>


</div>



<div className = "col">
<ul className = 'list-unstyled' id="brands">
<h6>Brands</h6>
<li>Supreme</li>
<li>Adidas</li>
<li>Vans</li>
<li>Nike</li>
</ul>


</div>



<div className = "col">
<ul id= "contacts" className = 'list-unstyled'>
<h6 >Contact Us</h6>
<li>Instagram</li>
<li>Facebook</li>

</ul>


</div>

</div>

<hr />

<div className="row">

<p className = "col-sm" id="copyright" >
&copy;{new Date().getFullYear()} KyoShiro | All rights reserved | Terms of Service | Privacy

</p>

</div>



</div>


 </div>

)

}



export default Footer;
