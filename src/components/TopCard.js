import { useState, useEffect, React } from 'react';
import { Link } from 'react-router-dom';
import pic1 from '../images/item.png';
import './prod.css'




export default function TopCard({topProp}) {

	const { _id, productName, productDescription, productPrice, ProfilePicture } = topProp;




	return(


		 
		<div>	
			
			<div id="product">

				<div className="card">
						
						 <img src={`https://secret-coast-94874.herokuapp.com/uploads/${ProfilePicture}`} />
				<div className="content">
					<h3>
						<h4>{productName}</h4>
					</h3>


						<Link className="forCSS btn btn-dark" to={`/products/${_id}`}>Details</Link>

					</div>
					
				</div>
				
			</div>			

		</div>
		




		)
}

